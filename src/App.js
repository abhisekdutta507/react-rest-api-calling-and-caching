import { useContext, useEffect } from 'react';
import { AppContext } from './App.controller';
import { cloneDeep } from './utils/object';
import './App.css';

const App = () => {
  const App = useContext(AppContext);

  console.log("@developers - App", cloneDeep(App));

  /**
   * @description it supports async response
   */
  useEffect(() => {
    // App.getMovies({ primaryReleaseYear: '2021' }).then(r => {
    //   console.log("@developers - r", cloneDeep(r));
    // });
  // eslint-disable-next-line
  }, []);

  return (
    <div className="App">
      Welcome to {App.name}!
    </div>
  );
}

export default App;
