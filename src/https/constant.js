export const RESPONSE = {
  data: undefined,
  isLoading: false,
  isError: false,
  error: undefined,
  getData: () => {}
};

export const HEADERS = {
  'Content-Type': 'application/json'
};

export const METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE'
};

export const MOVIES_DB_BASE_URL = 'https://api.themoviedb.org';

export const MOVIES_DB_API_KEY = '0a31ad5d96dfb51a2bcb7ce4009bbf98';
