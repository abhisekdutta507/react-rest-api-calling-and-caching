export const getData = ({
  url,
  method,
  headers,
  body
}, {
  state,
  setState,
  append
}) => {
  return new Promise((next) => {
    // should call the api again
    setState({ ...state, isLoading: true });

    fetch(url, {
      method,                           // *GET, POST, PUT, DELETE, etc.
      mode: 'cors',                     // no-cors, *cors, same-origin
      cache: 'no-cache',                // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin',       // include, *same-origin, omit
      headers,
      redirect: 'follow',               // manual, *follow, error
      referrerPolicy: 'no-referrer',    // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: body && JSON.stringify(body)        // body data type must match "Content-Type" header
    }).then((r) => {
      return r.json();
    }).then((r) => {
      const nextState = { ...state, data: r, error: undefined, isError: false, isLoading: false };
      setState(nextState);
      next(nextState);
    }).catch((e) => {
      const errorState = { ...state, data: undefined, error: e, isError: true, isLoading: false };
      setState(errorState);
      next(errorState);
    });
  });
};
