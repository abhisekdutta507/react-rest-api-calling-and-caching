import { useEffect, useState } from 'react';
import { RESPONSE, HEADERS, METHOD, MOVIES_DB_API_KEY, MOVIES_DB_BASE_URL } from './constant';
import { getData as executePromise } from './promise';
import { cloneDeep } from '../utils/object';

const Movies = {
    discover: {
        useFetch: ({
            page = 1,
            sortBy = 'primary_release_date.desc',
            primaryReleaseYear = '2021'
        }, {
            append = false,
            autoExecute = true
        }) => {
            let url = `${MOVIES_DB_BASE_URL}/3/discover/movie?api_key=${MOVIES_DB_API_KEY}&language=en-US&page=${page}&sort_by=${sortBy}&primary_release_year=${primaryReleaseYear}`;
            const method = METHOD.GET;
            const headers = cloneDeep(HEADERS);

            const [state, setState] = useState(RESPONSE);

            useEffect(() => {
                if (autoExecute) executePromise({ url, method, headers }, { state, setState, append });
            // eslint-disable-next-line
            }, []);

            const getData = async ({
                page,
                sortBy,
                primaryReleaseYear
            }) => {
                url = `${MOVIES_DB_BASE_URL}/3/discover/movie?api_key=${MOVIES_DB_API_KEY}&language=en-US&page=${page}&sort_by=${sortBy}&primary_release_year=${primaryReleaseYear}`;
                try {
                    return await executePromise({ url, method, headers }, { state, setState, append });
                } catch(e) {
                    // error handling
                }
            }
        
            return { ...state, getData };
        }
    },
    details: {
        useFetch: ({
            id
        }, {
            append = false,
            autoExecute = true
        }) => {
            let url = `${MOVIES_DB_BASE_URL}/3/movie/${id}?api_key=${MOVIES_DB_API_KEY}&language=en-US`;
            const method = METHOD.GET;
            const headers = cloneDeep(HEADERS);

            const [state, setState] = useState(RESPONSE);

            useEffect(() => {
                if (autoExecute) executePromise({ url, method, headers }, { state, setState, append });
            // eslint-disable-next-line
            }, []);

            const getData = async ({
                id
            }) => {
                url = `${MOVIES_DB_BASE_URL}/3/movie/${id}?api_key=${MOVIES_DB_API_KEY}&language=en-US`;
                try {
                    return await executePromise({ url, method, headers }, { state, setState, append });
                } catch(e) {
                    // error handling
                }
            }

            return { ...state, getData };
        }
    }
};

export default Movies;
