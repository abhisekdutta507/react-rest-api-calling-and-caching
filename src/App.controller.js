import { createContext } from 'react';
import App from './App';
import { Movies } from './https';

export const AppContext = createContext({
    name: '',
    movies: undefined,
    isMoviesLoading: false,
    getMovies: ({ page, sortBy, primaryReleaseYear }) => new Promise((next) => next()),
    movie: undefined,
    isMovieLoading: false,
    getMovie: ({ id }) => new Promise((next) => next())
});

const AppController = ({ ...rest }) => {
    const name = 'REST - API Calling & Caching';

    // the api hook must be here useHttps({ ...data }, initial call = true/false)
    const { data: movies, isLoading: isMoviesLoading, getData: getMovies } = Movies.discover.useFetch({ primaryReleaseYear: '2021' }, {});
    const { data: movie, isLoading: isMovieLoading, getData: getMovie } = Movies.details.useFetch({}, { autoExecute: false });

    return (
        <AppContext.Provider value={{
            name,
            movies,
            isMoviesLoading,
            getMovies,
            movie,
            isMovieLoading,
            getMovie,
            ...rest
        }}>
            <App />
        </AppContext.Provider>
    );
};

export default AppController;
